# Aves de Asturias

It's an open source project developed with React Native, Typescript, UI Lib, React Hooks, Mobx, i18next, React Navigation 6.

## Build

Install the dependencies (Node version > 14):

$ yarn install

Initialize your emulator:

$ yarn start

$ react-native run-android

## Frameworks & libraries

* [React Native](https://es.reactjs.org/) 
* [Typescript](https://www.typescriptlang.org/) 
* [UI Lib](https://wix.github.io/react-native-ui-lib/) 
* [React Navigator 6](https://reactnavigation.org/) 
* [i18next](https://www.i18next.com/) 
* [Mobx](https://mobx.js.org/README.html) 

# This Website

Allow CORS: Access-Control-Allow-Origin and run:

$ python -m SimpleHTTPServer

## Authors

* **Daniela Colombo & Mauro Bagnoli**

## Licence

MIT [licence](https://opensource.org/licenses/MIT) for more details.

