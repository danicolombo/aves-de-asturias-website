var language = "en"
var data

function getLanguage() {
	var urls = "json/" + language + ".json"

	$.getJSON(urls, function (response) {
		data = response
		$("#headerTagline").html(data.headerTagline)
		$("#headerDeveloped").html(data.headerDeveloped)
		$("#presentation").html(data.presentation)
		$("#presentationIntro").html(data.presentationIntro)
		$("#license").html(data.license)
		$("#licenseIntro1").html(data.licenseIntro1)
		$("#licenseIntro2").html(data.licenseIntro2)
		$("#licenseIntro3").html(data.licenseIntro3)
		$("#licenseIntro4").html(data.licenseIntro4)
		$("#screen").html(data.screen)
		$("#f1").html(data.f1)
		$("#f1p").html(data.f1p)
		$("#f2").html(data.f2)
		$("#f2p").html(data.f2p)
		$("#f3").html(data.f3)
		$("#f3p").html(data.f3p)
		$("#f4").html(data.f4)
		$("#f4p").html(data.f4p)
		$("#f5").html(data.f5)
		$("#f5p").html(data.f5p)
		$("#f6").html(data.f6)
		$("#f6p").html(data.f6p)
		$("#developed").html(data.developed)
		$("#popup").html(data.popup)
	})
}

function setLanguage(lang) {
	language = lang
	getLanguage()
}

const toggleEs = document.getElementById("toggleEs")
const toggleEn = document.getElementById("toggleEn")

toggleEs.addEventListener("click", function () {
	setLanguage("es")
})
toggleEn.addEventListener("click", function () {
	setLanguage("en")
})
